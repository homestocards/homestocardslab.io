
document.addEventListener("DOMContentLoaded", e => {
  const lines = document.querySelector(".log").children;

  for (let line of lines) {

    // Get the name of the character before :
    const i = line.textContent.indexOf(":");
    let charName = line.textContent.substring(0, i).replace(/\s/g, '');
    let char = charName.toLowerCase();
    let text = line.textContent.substring(i);

    // Find the character in the list based on name or key
    const key = Object.keys(characters)
              .find(c => {
                // If character name is the key, return this object
                if (c === char) return c;

                // If the names field isn't set return nothing
                if (!characters[c].names) return;

                // If the character name is found in a character's names field, return it
                return characters[c].names.find(n => n.toLowerCase() === char);
              });

    if (!key) continue;
    const character = characters[key];

    // Apply styles
    line.style.color = character.color;

    // Apply quirk
    if (character.quirk) {
      text = character.quirk(text);
      line.textContent = `${charName} : ${text}`;
    }

  }

});
