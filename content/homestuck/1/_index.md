---
titre: "Homestuck"
---

Un jeune homme se tient dans sa chambre. Il se trouve qu'aujourd'hui, le 13 avril 2009, c'est l'anniversaire de ce jeune homme.
Bien que la vie lui ait été donnée il y a treize ans, ce n'est qu'aujourd'hui qu'il recevra un nom !

Quel sera le nom de ce jeune homme ?
